# vRMS

This program creates an HTML statistics page from [`vrms`](https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=names&keywords=vrms) mails.

This programm connects to an IMAP account, checks the new mails, parses them and creates an HTML output in `public` folder.

## How to get `vrms` mails?

Install `vrms` on your computer, then simply create a cron task for:
```
vrms | mail -s "subject" address@example.org
```

Then use the credentials for that email address with this software.

## How to use it on your computer?

### Installation

Install Perl6 and some modules:

```
apt-get install -y git build-essential libssl-dev
git clone https://github.com/tadzik/rakudobrew ~/.rakudobrew
echo 'PATH=~/.rakudobrew/bin:$PATH' > ~/.bashrc
. ~/.bashrc
rakudobrew build moar
rakudobrew build zef
zef install --/test Template::Mojo File::Directory::Tree Terminal::Spinners JSON::Fast
zef install --/test --deps-only Net::IMAP
```

Install yarn:
```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
```

Install vRMS:

```
git clone https://framagit.org/framasoft/vrms
cd vrms/theme/default/front
yarn install
cd -
```

### Configuration

The configuration is provided by environment variables:

- `USER`: the user for the IMAP connection (mandatory)
- `PASSWORD`: his password (mandatory)
- `SERVER`: the IMAP server address (mandatory)
- `THEME`: the theme you want to use (optional, default is `default`)
- `HIDENAMES`: set to 1 if you want to hide the names of your server (optional, no default)
- `DEV`: set to 1 if you want to fetch all mails, not only the unread ones (optional, no default)

### Use

```
USER=foo@bar.org PASSWORD=s3cr3t SERVER=imap.bar.org ./vrms-web.pl6
```

## How to use it with Gitlab CI and Gitlab Pages?

Fork https://framagit.org/framasoft/vrms. Set the environment variables in the project configuration. Run a pipeline. That's all.
